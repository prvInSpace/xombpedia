
<table width=320px class="infobox">
  <caption><?= $term ?></caption>
  <tbody>
    <tr>
      <td colspan="2">
        <img src="https://cdn.images.express.co.uk/img/dynamic/36/590x/secondary/Westminster-Abbey-explosion-383678.jpg">  
      <td>
    </tr>
    <tr>
      <td colspan="2">Artist impression of the explosion<hr/></td>
    </tr>
    <tr style="text-align: left">
      <th>Date</th>
      <td>June 22, 2021</td>
    </tr>
    <tr style="text-align: left">
      <th>Location</th>
      <td>Houses of Parliament, London, United Kingdom</td>
    </tr>
    <tr style="text-align: left; vertical-align: top;">
      <th style="padding-top: 1.05em;">Result</th>
      <td>
        <ul style="padding-left: 0.3em;">
          <li>Increased focus on national security.</li>
          <li>The formation of the National Security Party.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<p>
The <strong>22nd of June Terrorist Attack</strong> was a terrorist bombing attack that took place in the House of Parliament during a debate leading up to the 5th General Election. Several audience members were injured by the explosion.
</p>
<p>
The perpetrator was later caught and revealed to be a libertarian right-wing extremist.
According to BBC sources, the perpetrator was known to intelligence services.<sup>[<a href="#bbc">1</a>]</sup>
</p>

<h2>Background</h2>
<p>

</p>

<h2>Events</h2>
<p>
After the main round of questioning had concluded, the audience were invited to ask questions.
Following some questions by <a href="?char=Hywel ap Iorwerth">1st Earl of Caernarfon</a> and the Liberal Democrat MP <a href="?char=Rake Wricliffe">Rake Wricliffe</a> a large explosion could be heard and the camera went black.<sup>[<a href="#bbc2">2</a>]</sup>
</p>
<p>
During the insuing chaos members of the audience and the participants took cover under chairs, tables and similar. Many of the party leaders were then escorted to safety by security.
</p>
<p>
The Chancellor of the Exchequer and leader of the Liberal Democrats, <a href="?char=Charles A. Gladstone">Sir Charles Gladstone</a> refused to take cover and continued speaking even after told to evacuate.
</p>

<h2>Aftermath</h2>
<p>
Not long after the incident, Conservative MP <a href="?char=John Bell">John Bell</a> left the party to form the National Security Party.
</p>

<h2>References</h2>
<ol>
  <li id="bbc"><a href="https://discord.com/channels/556271746592800772/556274782837014528/857008534712746034">BBC News article about the event</a></li>
  <li id="bbc2"><a href="https://discord.com/channels/556271746592800772/556273849247989760/856973521955717160">BBC coverage of the debate</a></li>
</ol>