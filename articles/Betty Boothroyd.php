<table class="infobox">
  <tbody>
    <tr>
      <td colspan="2">
        <span style="font-size: 125%;"><strong><?= $term ?></strong></span><br />
        Bot
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Official_portrait_of_Baroness_Boothroyd_crop_2.jpg/220px-Official_portrait_of_Baroness_Boothroyd_crop_2.jpg">  
    </td>
    </tr>
    <tr style="text-align: left">
      <th>Author</th>
      <td>Prv</td>
    </tr>
    <tr style="text-align: left">
      <th>Created</th>
      <td>July 2021</td>
    </tr>
    <tr style="text-align: left">
      <th>Repository</th>
      <td><a href="">Gitlab</a></td>
    </tr>
  </tbody>
</table>
<p>
  <strong><?= $term ?></strong> is a bot designed to collect and amalgamate data about XO's Model British Parliament.
  This data is then made available through a Discord bot and a HTTP Rest API. Data from Betty is used to generate some of the content on this site.
</p>
<h2>Design</h2>
<p>
  Betty is made using Kotlin.
  It has a set of JSON files containing extra information about the server which is not available through the office spreadsheet.
  It first reads this extra information and then goes ahead and fetches the data directly from the different Google Spreadsheets.
</p>